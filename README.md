
# Beer
This is a scraper built to collect data from Beer Advocate.
This is strictly for non-commerical purposes. 
This was built for an academic reserach project. 

# Requirements 
 - JSON
 - Pandas
 - Numpy
 - bs4
 - re 
 - requests

# How to use this repositiory?
 1. Clone this repositiory
 2. Create a reviews file inside install dir *optional
 3. Run beer.py from your cmd/term
 
 * you must do this to collect text review info
```bash
    git clone git@gitlab.com:infvie/beer.git
    cd beer
    mkdir reviews
    python beer.py
```

# API

## collectInfo()
INPUT: Boolean - True collects all text data from the reviews and False only collects aggregate statistics.

OUTPUT: CSVs with all beers high level statistics & optionally all the text info from reviews

This is a function that simplifies the process and calls on all the helper functions below to collect all data for you with out any configuration.

## getAllBeersOnSite()
INPUT: None

OUTPUT: DataFrame with cols [Beer IDs, Brewery IDs] and a CSV with cached results

This function takes in no parameters and it uses the two helper functions below to build a full dataset and a csv which uses
as a building point for the rest of the project. It calls the getAllBreweries and getAllBeers functions below.

### getAllBreweries()
INPUT: None

OUTPUT: DataFrame with col [Brewery ID's] and CSV with cached results

This function takes no parameters and goes through all US states and builds a list of all the breweries active on Beer 
Advocate.

### getAllBeers(BreweryID)
INPUT: Brewery ID

OUTPUT: Returns a DataFrame with all the beer id's for that specific brewery.

This function returns all the beers for a specific brewery.

## getBeerDetails(Beer ID, Brewery ID, Review)
INPUT: Beer ID & Brewery ID are the outputs provided by the function from getAllBeersOnSite 
       Review is a binary parameter setting the value TRUE returns all the reviews values and the text data for each review
       Review data creates a large json with the following conventions {Brewery ID : Beer ID: [User ID : Review *]}

OUTPUT: CSV of all beer details & reviews.json containing the text data of the reviews

# File Structure of Program

_* indicates a file made by beer.py_

       +--- beer.py 
       |
       +--- *breweries.csv 
       |
       +--- *beersOnSite.csv 
       |
       +--- *beerDetails.csv 
       |
       +--- *reviews 
           |
           +--- *JSON files for all textual data
           |
           +--- *CSV files with all numerical data
           |    
           +--- *mongo.db of text data collected from reviews

