import pandas as pd
import requests
from bs4 import BeautifulSoup
import re
import numpy as np
import json

def getAllBreweries():
    ''' this function returns all the breweries in US '''

    # check if files already exist locally
    try:
        breweries = pd.read_csv('breweries.csv', index_col=0)
        return breweries
    except:
        pass

    # beer advocate organizes the breweries all by state
    states = (["AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DC", "DE", "FL", "GA", 
              "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA",
              "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY",
              "NC", "ND", "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX",
              "UT", "VT", "VA", "WA", "WV", "WI", "WY"])

    # create an empty container to store the brewery ids
    breweries = pd.Series()
    
    # iterate through every state and keep track of their breweries
    n = 0
    for state in states:
        html = requests.get(f'https://www.beeradvocate.com/place/list/?start=0&c_id=US&s_id={state}&brewery=Y&sort=name')
        count = re.findall('out of (\d+)',html.text)[0]
        count = int(count)
        brew = re.findall('\/beer\/profile\/(\d+)\/',html.text)
        breweries = pd.concat([breweries,pd.Series(brew)],ignore_index=True)
        if count > 20:
            for i in range(20,count,20):
                html = requests.get(f'https://www.beeradvocate.com/place/list/?start={i}&c_id=US&s_id={state}&brewery=Y&sort=name')
                brew = re.findall('\/beer\/profile\/(\d+)\/',html.text)
                breweries = pd.concat([breweries,pd.Series(brew)],ignore_index=True)
        n += 1
        print(f'\rThrough {n} out of 50 states!')
    
    # save a copy to avoid making calls in next run
    breweries.to_csv('breweries.csv')
    
    return breweries


def getAllBeers(brewid):
    '''gets all the beers for a specific brewery'''
    
    html = requests.get(f'https://www.beeradvocate.com/beer/profile/{brewid}/')
    beerids = re.findall(f'\/beer\/profile\/{brewid}\/(\d+)\/',html.text)
    return pd.DataFrame(beerids,columns=['Beer IDs'])

def allBeersOnSite():
    '''using the getAllBreweries and getAllBeers helper fucntion this is able to collect all the
    beers in the US'''
    
    print('Collecting all the beers on the site...\n')
    try:
        beers = pd.read_csv('beersOnSite.csv',index_col=0)
        return beers
    except:
        pass

    beersOnSite = pd.DataFrame()
    breweryids = getAllBreweries()
    r = len(breweryids.index)
    n = 0

    for brewid in breweryids.iloc[:,0]:
        beers = getAllBeers(brewid)
        beers['Brewery ID'] = brewid
        beersOnSite = pd.concat([beersOnSite,beers],ignore_index=True)
        n += 1
        print(f'\rProgress: {100*n/r:.2f}%') 
        
    # cache results
    beersOnSite.to_csv('beersOnSite.csv')
    return beersOnSite


def getBeerDetails(beerid,breweryid,collect_reviews=False):
    ''' collects beer specific details and includes optional parameter for
    collecting all review details - these are stored as json files in local file
    directory see README.md (on www.github.com/infvie/beer) for more info'''

    html = requests.get(f'https://www.beeradvocate.com/beer/profile/{breweryid}/{beerid}/')
    soup = BeautifulSoup(html.text,'lxml')
    
    beerDetails = pd.DataFrame(index = [beerid])

    beerDetails['Brewery'] = breweryid

    beerDetails['Average Rating'] = soup.find_all('span',{'class':'ba-ravg'})[0].text
    beer_stats = soup.find_all('div',{'id':'item_stats'})[0].text
    beer_stats = re.findall('#?(\S+)',beer_stats)
    beerDetails['Rank'] = beer_stats[1].replace(',','') 
    beerDetails['# of Reviews'] = beer_stats[3].replace(',','') 
    beerDetails['# of Ratings'] = beer_stats[5].replace(',','')
    beerDetails['pDev'] = beer_stats[7].replace('%','')
    beerDetails['Wants'] = beer_stats[9].replace(',','')
    beerDetails['Gots'] = beer_stats[11].replace(',','')
    beerDetails['Trades'] = beer_stats[13].replace(',','')
    beer_info = soup.find_all('div',{'id':'info_box'})[0].text
    beerDetails['Style'] = re.findall('Style:(.+)\n',beer_info)[0].strip()
    beerDetails['ABV'] = re.findall('\(ABV\):(.+)\n',beer_info)[0]
    beerDetails['Availability'] = re.findall('Availability:(.+)\n',beer_info)[0].strip()
    beerDetails['Notes'] = re.findall('Notes \/ Commercial Description:\s+(.*)',beer_info)[0].strip()
    
    # evoke the review collection if required parameter is true
    if collect_reviews:
        collectReviews(beerid,breweryid,int(beerDetails['# of Ratings']))

    return beerDetails

def collectReviews(beerid,breweryid,count):
    '''This collects the review detail. It stores the written portion of the reviews as JSON'''

    # this is where we will store the reviews
    reviews = pd.DataFrame()
    url = f'https://www.beeradvocate.com/beer/profile/{breweryid}/{beerid}/'
    
    # repeat for all reviews 
    for i in range(0,count,25):
        current_url = f'{url}?view=beer&sort=&start={i}'
        html = requests.get(current_url).text
        soup = BeautifulSoup(html,'lxml')
        soup = soup.find_all('div',{'id':'rating_fullview_container'})

        # containers 
        users = list()
        rating = list()
        look = list()
        smell = list()
        taste = list()
        feel = list()
        overall = list()
        text = dict()

        
        for i in soup:
            users.append(i['ba-user'])
            rating.append(i.find_all('span',{'class':'BAscore_norm'})[0].text)
            
            i = i.text
            
            try:
                overall.append(re.findall('overall: (\d?.?\d+)',i)[0])
                look.append(re.findall('look: (\d?.?\d+) ', i)[0])
                smell.append(re.findall('smell: (\d?.?\d+) ', i)[0])
                taste.append(re.findall('taste: (\d?.?\d+) ', i)[0])
                feel.append(re.findall('feel: (\d?.?\d+) ', i)[0])
                try:
                    text[users[-1]] = re.findall('overall: \d?.?\d+(.+) characters',i.replace('\n',' '))[0]
                except:
                    pass
            except:
                overall.append('')
                look.append('')
                smell.append('')
                taste.append('')
                feel.append('')
                
        reviewsCurrentlyVisible =  pd.DataFrame([look,smell,taste,feel,overall,rating]).transpose()
        reviewsCurrentlyVisible.columns = ['Look','Smell','Taste','Feel','Overall','Rating']
        reviewsCurrentlyVisible.index = users
        reviews = pd.concat([reviews,reviewsCurrentlyVisible])
        
        # make json file here 
        with open(f'./reviews/{breweryid}-{beerid}.json','w') as f:
            f.write(json.dumps(text))
        
    reviews.to_csv(f'./reviews/{breweryid}-{beerid}.csv',mode='w')


def collectInfo(textdata = False):
    print('Collecting all breweries and beers in the US!')
    beersOnSite = allBeersOnSite()
    header = (['BeerID','Brewery', 'Average Rating', 'Rank', '# of Reviews', '# of Ratings', 'pDev', 'Wants',
            'Gots', 'Trades', 'Style', 'ABV', 'Availability', 'Notes'])
    with open('allBeersDetails.csv','w') as f:
        f.write(','.join(header))
    n = len(beersOnSite.index)
    for i in beersOnSite.index:
        beerDetails = getBeerDetails(beersOnSite.loc[i,'Beer IDs'],beersOnSite.loc[i,'Brewery ID'],textdata)
        beerDetails.to_csv('allBeersDetails.csv',mode='a',header=False)
        print(f'{i+1} of {n} beers done!')

if __name__ == "__main__":
    collectInfo(True)
